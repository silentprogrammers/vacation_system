﻿using System;

namespace VacationsCore
{
    public class VacationDay
    {
        public int Id { get; set; }
        public DateTime Day { get; set; }
        public virtual User User { get; set; }

        public VacationDay() { }

        public VacationDay(User user)
        {
            User = user;
        }

        public VacationDay(DateTime day, User user)
        {
            Day = day;
            User = user;
        }

        public override string ToString()
        {
            return $"{Id} - {Day:d}";
        }

        public override int GetHashCode()
        {
            return Day.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return Day.Equals(obj);
        }
    }
}
