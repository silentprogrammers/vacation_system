﻿using System;
using System.Collections.Generic;

namespace VacationsCore.Repository
{
    public interface IRepository<TEntity> where TEntity : class
    {
        void Delete(TEntity entityToDelete);
        void Delete(int id);
        IEnumerable<TEntity> Get(Func<TEntity, bool> predicate = null);
        TEntity GetById(int id);
        void Insert(TEntity entity);
        void Update(TEntity entityToUpdate);
    }
}
