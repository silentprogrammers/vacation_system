﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VacationsCore.Services
{
    public interface IVacationsValidationService
    {
        bool ValidateVacations(List<VacationDay> vacations);
    }
}
