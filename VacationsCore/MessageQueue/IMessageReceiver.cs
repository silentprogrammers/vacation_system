﻿namespace VacationsCore.MessageQueue
{
    public interface IMessageReceiver
    {
       User GetUsersFromQueue();
    }
}
