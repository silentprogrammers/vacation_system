﻿namespace VacationsCore.MessageQueue
{
    public interface IMessagePublisher
    {
        void PublishMessage(User user);        
    }
}
