﻿using System;

namespace VacationsCore
{
    public interface ILogger<T>
    {
        void Debug(string format, params object[] formatArgs);
        void Info(string format, params object[] formatArgs);
        void Warning(string format, params object[] formatArgs);
        void Error(Exception exception, string format, params object[] formatArgs);
        void Error(string format, params object[] formatArgs);
    }
}
