﻿using System;

namespace VacationsCore
{
    public class Login
    {
        public int Id { get; set; }
        public string LoginString { get; set; }
        public string PasswordHash { get; set; }
        public virtual User User { get; set; }

        public Login() { }

        public Login(User user)
        {
            User = user;
        }

        public Login(string loginString, string passwordHash, User user)
        {
            LoginString = loginString;
            PasswordHash = passwordHash;
            User = user;
        }

        public override string ToString()
        {
            return $"{Id} - {LoginString} - {PasswordHash}";
        }

        public override int GetHashCode()
        {
            return LoginString.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Login lObj))
                return false;
            return string.Compare(lObj.LoginString, LoginString, StringComparison.CurrentCultureIgnoreCase) == 0;
        }
    }
}
