﻿using System;

namespace VacationsCore
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}
