﻿using System;
using System.Collections.Generic;

namespace VacationsCore
{
    public class User : IEntity
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }

        public virtual List<Login> Logins { get; set; }
        public virtual List<VacationDay> VacationDays { get; set; }

        public User() 
        {
        }

        public User(int id, string fullName, string email)
        {
            Id = id;
            FullName = fullName;
            Email = email;
        }

        public override string ToString()
        {
            var userString = $"{Id} - {FullName} - {Email}";
            var loginsString = Logins == null ? "" : string.Join("; ", Logins);
            var vacationDaysString = VacationDays == null ? "" : string.Join("; ", VacationDays);

            return string.Join(Environment.NewLine, userString, loginsString, vacationDaysString);
        }

        public override int GetHashCode()
        {
            return FullName.GetHashCode() + Email.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (!(obj is User uObj))
                return false;
            return string.Compare(uObj.FullName, FullName, StringComparison.CurrentCultureIgnoreCase) == 0
                   && string.Compare(uObj.Email, Email, StringComparison.CurrentCultureIgnoreCase) == 0;
        }
    }
}
