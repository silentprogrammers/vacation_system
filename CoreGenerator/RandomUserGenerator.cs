﻿using Bogus;
using VacationsCore;

namespace CoreGenerator
{
    public class RandomUserGenerator
    {
        public static User Generate()
        {
            var userFaker = CreateFaker();
            return userFaker.Generate();
        }

        private static Faker<User> CreateFaker()
        {
            var userFaker = new Faker<User>("ru")
                .CustomInstantiator(f => new User())
                .RuleFor(u => u.FullName, (f, u) => f.Person.FullName)
                .RuleFor(u => u.Email, (f, u) => f.Person.Email);

            return userFaker;
        }
    }
}