﻿using System.Collections.Generic;
using VacationsCore;

namespace CoreGenerator
{
    public static class FullUserGenerator
    {
        public static List<User> Generate(int dataCount)
        {
            var users = new List<User>();

            for (int i = 0; i < dataCount; i++)
            {
                users.Add(Generate());
            }

            return users;
        }

        public static User Generate()
        {
            var user = RandomUserGenerator.Generate();
            user.Logins = new List<Login> { RandomLoginGenerator.Generate(user) };
            user.VacationDays = RandomVacationDayGenerator.GenerateFullVacation(user);
            return user;
        }
    }
}