﻿using System.Collections.Generic;
using System.Linq;
using Bogus;
using VacationsCore;

namespace CoreGenerator
{
    public class RandomVacationDayGenerator
    {
        public static List<VacationDay> GenerateFullVacation(User user)
        {
            var vacationDays = Generate(user, 14);
            var faker = new Faker("ru");

            var workInterval = faker.Random.Int(7 * 2, 7 * 6);
            var day = vacationDays.Last().Day.AddDays(workInterval);
            for (int i = 0; i < 7; i++)
            {
                var vacationDay = new VacationDay(user)
                {
                    Day = day.AddDays(i)
                };
                vacationDays.Add(vacationDay);
            }

            workInterval = faker.Random.Int(7 * 2, 7 * 6);
            day = vacationDays.Last().Day.AddDays(workInterval);
            for (int i = 0; i < 7; i++)
            {
                var vacationDay = new VacationDay(user)
                {
                    Day = day.AddDays(i)
                };
                vacationDays.Add(vacationDay);
            }

            return vacationDays;
        }

        public static List<VacationDay> Generate(User user, int dataCount)
        {
            var vacationDays = new List<VacationDay>();
            var vacationDayFaker = CreateFaker(user);

            vacationDays.Add(vacationDayFaker.Generate());
            var day = vacationDays[0].Day;
            for (int i = 1; i < dataCount; i++)
            {
                var vacationDay = new VacationDay(user)
                {
                    Day = day.AddDays(i)
                };
                vacationDays.Add(vacationDay);
            }

            return vacationDays;
        }

        public static VacationDay Generate(User user)
        {
            var vacationDayFaker = CreateFaker(user);
            return vacationDayFaker.Generate();
        }

        private static Faker<VacationDay> CreateFaker(User user)
        {
            var loginFaker = new Faker<VacationDay>("ru")
                .CustomInstantiator(f => new VacationDay(user))
                .RuleFor(u => u.Day, (f, u) => f.Date.Soon(14));

            return loginFaker;
        }
    }
}
