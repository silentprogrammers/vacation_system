﻿using Bogus;
using VacationsCore;

namespace CoreGenerator
{
    public static class RandomLoginGenerator
    {
        public static Login Generate(User user)
        {
            var loginFaker = CreateFaker(user);
            var result = loginFaker.Generate();
            result.LoginString = user.Email.Substring(0, user.Email.IndexOf('@'));
            return result;
        }

        private static Faker<Login> CreateFaker(User user)
        {
            var loginFaker = new Faker<Login>("ru")
                .CustomInstantiator(f => new Login(user))
                .RuleFor(u => u.PasswordHash, (f, u) => f.Random.Hash());

            return loginFaker;
        }
    }
}
