﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using VacationsCore;
using VacationsCore.MessageQueue;

namespace MessageClient
{
    public class MessageReceiver:IMessageReceiver
    {        
        private ConnectionFactory Factory { get; }
        private IConnection Connection { get; }
        private IModel Channel { get; }
        public MessageReceiver()
        {
            Factory = new ConnectionFactory() 
            { 
                HostName = "hawk.rmq.cloudamqp.com", 
                VirtualHost = "hzutqcym", 
                UserName = "hzutqcym", 
                Password = "l6370CKOHQKUygeKeDb1n628REgg6Z3v",
                AutomaticRecoveryEnabled=true
            };
            Connection = Factory.CreateConnection();
            Channel = Connection.CreateModel();
        }

        public User GetUsersFromQueue()
        {
            List<User> users = new List<User>();
            MessageSettings rqs = new MessageSettings();
            //rqs.GetSettingsAsync();
            User user = null;

            Channel.QueueDeclare(queue: "mainRoute",
                durable: true,
                exclusive: false,
                autoDelete: false,
                arguments: null);

            var consumer = new EventingBasicConsumer(Channel);
            consumer.Received += HandlingMessage;
            Channel.BasicConsume(queue: "mainRoute", autoAck: false, consumer: consumer);//(consumer, "mainRoute");
            return user;
        }

        private void HandlingMessage(object model, BasicDeliverEventArgs ea)
        {
            var body = Encoding.UTF8.GetString(ea.Body.ToArray());
            var message = JsonConvert.DeserializeObject<User>(body);
            Channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
            Console.WriteLine(message.ToString());
            //SendMail(message);            
        }

        private void SendMail(User user)
        {
            // отправитель
            MailAddress from = new MailAddress("address", "Mike");
            // кому отправляем
            MailAddress to = new MailAddress("address");            
            MailMessage m = new MailMessage(from, to);            
            m.Subject = "Тест";            
            m.Body = user.ToString();           
            m.IsBodyHtml = true;           
            SmtpClient smtp = new SmtpClient("smtp.mail.ru", 2525);           
            smtp.Credentials = new NetworkCredential("address", "pass");
            smtp.EnableSsl = true;
            smtp.Send(m);
        }
    }
}
