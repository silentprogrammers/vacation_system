FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build
WORKDIR /src
COPY ["Vacations.API/Vacations.API.csproj", "Vacations.API/"]
RUN dotnet restore "Vacations.API/Vacations.API.csproj"
COPY . .
WORKDIR "/src/Vacations.API"
RUN dotnet build "Vacations.API.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Vacations.API.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Vacations.API.dll"]
