﻿using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using VacationImplementation.Services;
using VacationsCore;
using VacationsCore.Services;

namespace VacationImplementation.Tests.Services
{
    //[TestFixture(typeof(BasicVacationsValidationService))]
    //[TestFixture(typeof(ExtendedVacationsValidationService))]
    
    public abstract class VacationsValidationServiceTests//<TService> where TService : IVacationsValidationService
    {
        internal abstract IVacationsValidationService CreateIVacationsValidationService();
        //{
        //    vService = new TService();
        //}
        //internal VacationsValidationServiceTests(TService service)
        //{
        //    vService = service;
        //}

        [Test]
        public void ValidateVacations_Null_IsNotError()
        {
            // Arrange
            var validationService = CreateIVacationsValidationService();
            List<VacationDay> vacations = null;
            var expectedResult = true;

            // Act
            var result = validationService.ValidateVacations(
                vacations);

            // Assert
            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void ValidateVacations_WithDuplicates_ReturnFalse()
        {
            // Arrange
            var basicVacationsValidationService = CreateIVacationsValidationService();
            List<VacationDay> vacationsWithDuplicates = new List<VacationDay>();
            for (int i = 1; i < 29; i++)
            {
                VacationDay day = new VacationDay();
                day.Day = new DateTime(2222, 2, 2);
                vacationsWithDuplicates.Add(day);
            }

            var expectedResult = false;

            // Act
            var result = basicVacationsValidationService.ValidateVacations(vacationsWithDuplicates);

            // Assert
            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void ValidateVacations_LessThen28_ReturnFalse()
        {
            // Arrange
            var basicVacationsValidationService = CreateIVacationsValidationService();
            List<VacationDay> vacations27 = new List<VacationDay>();
            for (int i = 1; i < 28; i++)
            {
                VacationDay day = new VacationDay();
                day.Day = new DateTime(2222, 2, i);
                vacations27.Add(day);
            }
            var expectedResult = false;

            // Act
            var result = basicVacationsValidationService.ValidateVacations(vacations27);

            // Assert
            Assert.AreEqual(expectedResult, result);
        }
    }
}
