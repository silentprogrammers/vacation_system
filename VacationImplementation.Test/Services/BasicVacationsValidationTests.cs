﻿using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using VacationImplementation.Services;
using VacationsCore;
using VacationsCore.Services;

namespace VacationImplementation.Tests.Services
{
    //[TestFixture]
    public class BasicVacationsValidationTests : VacationsValidationServiceTests
    {
        //[OneTimeSetUp]
        internal void CreateBasicVacationsValidation() { }

        internal override IVacationsValidationService CreateIVacationsValidationService()
        {
            return new BasicVacationsValidationService();
        }

        [Test]
        public void ValidateVacations_CorrectExample_ReturnTrue()
        {
            // Arrange
            var vacationsValidationService = this.CreateIVacationsValidationService();
            List<VacationDay> vacations28 = new List<VacationDay>();
            for (int i = 1; i < 29; i++)
            {
                VacationDay day = new VacationDay();
                day.Day = new DateTime(2222, 3, i);
                vacations28.Add(day);
            }
            var expectedResult = true;

            // Act
            var result = vacationsValidationService.ValidateVacations(vacations28);

            // Assert
            Assert.AreEqual(expectedResult, result);
        }
    }
}
