﻿using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using VacationImplementation.Services;
using VacationsCore;
using VacationsCore.Services;

namespace VacationImplementation.Tests.Services
{
    //[TestFixture]
    public class ExtendedVacationsValidationTests : VacationsValidationServiceTests
    {
        //[OneTimeSetUp]
        internal void CreateExtendedVacationsValidation() { }

        internal override IVacationsValidationService CreateIVacationsValidationService()
        {
            var moq = new Mock<IIntegratedAssessment>();
            moq.Setup(i => i.Calc(It.IsAny<List<VacationDay>>()))
                .Returns(ExtendedVacationsValidationService.MinCorrectIntegratedAssessment);
            return new ExtendedVacationsValidationService(new BasicVacationsValidationService(), moq.Object);
        }

        internal IVacationsValidationService CreateExtendedVacationsValidation(IIntegratedAssessment integratedAssessment)
        {
            return new ExtendedVacationsValidationService(new BasicVacationsValidationService(), integratedAssessment);
        }

        [Test]
        public void ValidateVacations_CorrectExample_ReturnTrue()
        {
            // Arrange
            List<VacationDay> correctVacations = new List<VacationDay>();
            for (int i = 1; i < 29; i++)
            {
                VacationDay day = new VacationDay();
                day.Day = new DateTime(2222, 3, i);
                correctVacations.Add(day);
            }
            var moq = new Mock<IIntegratedAssessment>();
            moq.Setup(i => i.Calc(correctVacations)).Returns(ExtendedVacationsValidationService.MinCorrectIntegratedAssessment);
            var vacationsValidationService = CreateExtendedVacationsValidation(moq.Object);

            // Act
            var result = vacationsValidationService.ValidateVacations(correctVacations);

            // Assert
            Assert.AreEqual(true, result);
        }

        [Test]
        public void ValidateVacations_IntegratedAssessmentLessMinCorrectIntegratedAssesment_ReturnFalse()
        {
            // Arrange
            List<VacationDay> correctVacations = new List<VacationDay>();
            for (int i = 1; i < 29; i++)
            {
                VacationDay day = new VacationDay();
                day.Day = new DateTime(2222, 3, i);
                correctVacations.Add(day);
            }
            var moq = new Mock<IIntegratedAssessment>();
            moq.Setup(i => i.Calc(It.IsAny<List<VacationDay>>())).Returns(ExtendedVacationsValidationService.MinCorrectIntegratedAssessment-1);
            var extendedVacationsValidationService = CreateExtendedVacationsValidation(moq.Object);

            // Act
            var result = extendedVacationsValidationService.ValidateVacations(correctVacations);

            // Assert
            Assert.AreEqual(false, result);
        }
    }
}
