﻿using CoreGenerator;
using Logging;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using VacationImplementation.Serialization;
using VacationsCore;

namespace VacationImplementation.Tests.Serialization
{
    [TestFixture]
    public class CSVSerializationTests
    {
        [Test]
        public void Serialization_SerializationUsers_OK()
        {
            // Arrange
            List<User> users = new List<User>();
            for (int i = 0; i < 4; i++)
            {
                User user = FullUserGenerator.Generate();
                users.Add(user);
            }

            // Act
            CSVSerialization serialization = new CSVSerialization(new NLogLogger<CSVSerialization>());
            string stringCSV = serialization.Serialization(users);
            string[] usersCSV = stringCSV.Split(Environment.NewLine);

            // Assert
            Assert.AreEqual(usersCSV.Length, 4);
        }

        [Test]
        public void Serialization_DeserializationUsers_OK()
        {
            // Arrange
            string stringCSV = "5589;Игорь Ждановвв;Igor_Zhdanov@mail.ru;L;4663;Igor_Zhdanov;a3338ef41954c4a1eb5c06195174d922819189c1;V;121698;31.08.2020 1:35:21;121699;13.07.2020 1:35:21;121700;26.05.2020 1:35:21;121719;30.08.2020 1:35:21;121721;29.08.2020 1:35:21;121722;17.07.2020 1:35:21;121723;23.05.2020 1:35:21;121724;24.05.2020 1:35:21;121725;02.09.2020 1:35:21;121726;22.05.2020 1:35:21;121727;25.05.2020 1:35:21;121728;27.05.2020 1:35:21;121729;29.05.2020 1:35:21;121730;30.05.2020 1:35:21;121731;04.06.2020 1:35:21;121732;01.06.2020 1:35:21;121733;31.05.2020 1:35:21;121734;03.09.2020 1:35:21;121735;01.09.2020 1:35:21;121736;02.06.2020 1:35:21;121737;03.06.2020 1:35:21;121738;11.07.2020 1:35:21;121739;28.05.2020 1:35:21;121740;12.07.2020 1:35:21;121741;14.07.2020 1:35:21;121742;15.07.2020 1:35:21;121743;16.07.2020 1:35:21;121744;28.08.2020 1:35:21;";
            stringCSV += Environment.NewLine + "5645;Арина Кириллова;Arina75@mail.ru;L;4719;Arina75;c897fca388afdd932d5c5270b3f5ab6e0ee31a47;V;123266;23.06.2020 0:15:53;123267;20.07.2020 0:15:53;123268;21.07.2020 0:15:53;123287;11.05.2020 0:15:53;123289;22.06.2020 0:15:53;123290;20.06.2020 0:15:53;123291;25.07.2020 0:15:53;123292;26.07.2020 0:15:53;123293;20.05.2020 0:15:53;123294;19.05.2020 0:15:53;123295;18.05.2020 0:15:53;123296;17.05.2020 0:15:53;123297;12.05.2020 0:15:53;123298;13.05.2020 0:15:53;123299;14.05.2020 0:15:53;123300;15.05.2020 0:15:53;123301;16.05.2020 0:15:53;123302;24.07.2020 0:15:53;123303;23.07.2020 0:15:53;123304;22.07.2020 0:15:53;123305;21.05.2020 0:15:53;123306;22.05.2020 0:15:53;123307;23.05.2020 0:15:53;123308;24.05.2020 0:15:53;123309;17.06.2020 0:15:53;123310;18.06.2020 0:15:53;123311;19.06.2020 0:15:53;123312;21.06.2020 0:15:53;";

            // Act
            CSVSerialization serialization = new CSVSerialization(new NLogLogger<CSVSerialization>());
            List<User> users = serialization.Deserialization(stringCSV);

            // Assert
            Assert.AreEqual(users.Count, 2);
        }
    }
}
