﻿using LiteDB;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq.Expressions;
using VacationsCore;
using VacationsCore.Repository;

namespace RepositoryLiteDbImpl
{
    public abstract class RepositoryLiteDb<TEntity> : IRepository<TEntity> where TEntity : class, IEntity
    {
        // Потом добвить в       App.config
        //<connectionStrings>
        //    <add name = "LiteDB" connectionString="Filename=C:\database.db;Password=1234" />
        //</connectionStrings>
        //System.Configuration.ConfigurationManager.ConnectionStrings["LiteDB"].ConnectionString = "";
        //string connectionString = @"Filename=\liteDb.db;Password=1234";
        //string connectionString = @"Filename=UsersLiteDb.db;Password=1234;Mode=Shared";
        //string connectionString = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, @"UsersLiteDb.db");
        protected static ILiteRepository LiteRepository { get; set; }
        //private static ILiteRepository _repository;

        public RepositoryLiteDb(string connectionString, BsonMapper mapper)
        {
            //LiteRepository.
            var combinedConnectionString = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, connectionString);
            if (LiteRepository == null) LiteRepository = new LiteRepository(combinedConnectionString, mapper);
        }

        public void Delete(TEntity entity)
        {
            LiteRepository.Delete<TEntity>(entity.Id);
        }

        public void Delete(int id)
        {
            LiteRepository.Delete<TEntity>(id);
        }

        public abstract IEnumerable<TEntity> Get(Func<TEntity, bool> pred);
        //    {
        //        if (pred == null) return LiteRepository.Query<TEntity>().ToEnumerable();
        //    Expression<Func<TEntity, bool>> expression = Expression.Lambda<Func<TEntity, bool>>(Expression.Call(pred?.Method));
        //        return LiteRepository.Query<TEntity>().Where(expression).ToEnumerable();
        //}
        //public abstract IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> filter);


        public TEntity GetById(int id)
        {
            return LiteRepository.SingleById<TEntity>(id);
        }

        public void Insert(TEntity entity)
        {
            LiteRepository.Insert<TEntity>(entity);
        }

        public void Update(TEntity entity)
        {
            LiteRepository.Update<TEntity>(entity);
        }
    }
}
