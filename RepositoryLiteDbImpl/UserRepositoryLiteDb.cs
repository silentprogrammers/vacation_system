﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using LiteDB;
using VacationsCore;

namespace RepositoryLiteDbImpl
{
    public class UserRepositoryLiteDb : RepositoryLiteDb<User>
    {
        public UserRepositoryLiteDb(string connectionString, BsonMapper mapper) : base(connectionString, mapper)
        {
        }

        public static BsonMapper GetMapper()
        {
            var mapper = new BsonMapper();
            mapper.Entity<Login>()
                .Ignore(x => x.User);
            mapper.Entity<VacationDay>()
                .Ignore(x => x.User);
            return mapper;
        }

        public override IEnumerable<User> Get(Func<User, bool> predicate = null)
        {
            if (predicate == null) return LiteRepository.Query<User>().ToEnumerable();
            Expression<Func<User, bool>> expression =
                Expression.Lambda<Func<User, bool>>(Expression.Call(predicate?.Method));
            return LiteRepository.Query<User>().Where(expression).ToEnumerable();
        }

        //public override IEnumerable<User> Get(Expression<Func<User, bool>> expression = null)
        //{
        //    if (expression == null) return LiteRepository.Query<User>().ToEnumerable();
        //    //if (pred == null) return LiteRepository.Query<User>().ToEnumerable();
        //    //Expression<Func<User, bool>> expression = Expression.Lambda<Func<User, bool>>(Expression.Call(pred?.Method));
        //    return LiteRepository.Query<User>().Where(expression).ToEnumerable();
        //}
    }
}
