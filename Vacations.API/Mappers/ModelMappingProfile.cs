﻿using AutoMapper;
using Vacations.API.Models;
using VacationsCore;

namespace Vacations.API.Mappers
{
    public class ModelMappingProfile : Profile
    {
        public ModelMappingProfile()
        {
            CreateMap<User, UserModel>();
            CreateMap<UserModel, User>().AfterMap(FixCircularLinks); // TODO: configure
            CreateMap<VacationDay, VacationDayModel>();
            CreateMap<VacationDayModel, VacationDay>(); // TODO: configure

            // TODO: Create Unit Test
            //var mappingProfile = new ModelMappingProfile();
            //var config = new MapperConfiguration(q=>q.AddProfile(mappingProfile));
            //var mapper = new Mapper(config);
            //(mapper as IMapper).ConfigurationProvider.AssertConfigurationIsValid();
        }

        private static void FixCircularLinks(UserModel userModel, User user)
        {
            if (user.VacationDays != null)
            {
                foreach (var vacationDay in user.VacationDays)
                    vacationDay.User = user;
            }

            if (user.Logins != null)
            {
                foreach (var login in user.Logins)
                    login.User = user;
            }
        }
    }
}
