﻿using System;
using System.Collections.Generic;

namespace Vacations.API.Models
{
    public class UserModel
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public List<VacationDayModel> VacationDays { get; set; }

        [Obsolete("Only for serialization")]
        public UserModel()
        {
        }

        public UserModel(int id, string fullName, string email, List<VacationDayModel> vacationDays)
        {
            Id = id;
            FullName = fullName;
            Email = email;
            VacationDays = vacationDays;
        }
    }
}
