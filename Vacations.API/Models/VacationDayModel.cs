﻿using System;

namespace Vacations.API.Models
{
    public class VacationDayModel
    {
        public DateTime Day { get; set; }

        [Obsolete("Only for serialization")]
        public VacationDayModel() { }
    }
}
