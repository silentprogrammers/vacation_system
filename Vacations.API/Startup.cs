﻿using System;
using AutoMapper;
using Logging;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using VacationImplementation.Repository;
using Vacations.API.Controllers;
using Vacations.API.Mappers;
using VacationsCore;
using VacationsCore.Repository;
using Microsoft.OpenApi.Models;
using RepositoryLiteDbImpl;
using VacationImplementation.Services;
using VacationsCore.Services;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace Vacations.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options =>
                {
                    options.LoginPath = new Microsoft.AspNetCore.Http.PathString("/Account/Login");
                });
            //services.AddControllers();
            services.AddControllersWithViews();
            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "My API", Version = "v1" });
            });

            var userRepository = GetRepositoryFromAppSettings();
            services.AddSingleton(typeof(IRepository<User>), userRepository);
            services.AddAutoMapper(typeof(ModelMappingProfile));
            services.AddScoped<IVacationsValidationService>((v)=> new ExtendedVacationsValidationService(
                new BasicVacationsValidationService(),
                new IntegratedAssessment()));
            services.AddSingleton<VacationsCore.ILogger<UsersController>>(new NLogLogger<UsersController>());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseStaticFiles();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();
            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
                //c.RoutePrefix = string.Empty;
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                //endpoints.MapControllers();
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }

        private IRepository<User> GetRepositoryFromAppSettings()
        {
            //string connectionString = string.Empty;
            IRepository<User> repository;
            if (System.Enum.TryParse(Configuration.GetValue<string>("DbSettings:UsedDb"), ignoreCase: true, out DbEnum usedDb))
            {
                string connectionString = Configuration.GetValue<string>($"DbSettings:DbConnectionStrings:{usedDb}");
                if (!string.IsNullOrWhiteSpace(connectionString))
                {
                    repository = FromDbEnum(usedDb, connectionString);
                }
                else throw new Exception("Can't read ConnectionStrings from appsettings file");
            }
            else  throw new Exception("Can't read DbSettings from appsettings file");

            return repository;
        }

        private enum DbEnum
        {
            EfDb,
            LiteDb,
            FakeDb
        }

        private static IRepository<User> FromDbEnum(DbEnum db, string connectionString) =>
            db switch
            {
                DbEnum.EfDb => new VacationImplementation.Repository.EFRepository.UserRepository(connectionString),
                DbEnum.LiteDb => new UserRepositoryLiteDb(connectionString, UserRepositoryLiteDb.GetMapper()),
                DbEnum.FakeDb => new VacationImplementation.Repository.FakeUserRepository(),
                _ => throw new ArgumentException(message: "invalid enum value", paramName: nameof(db)),
            };
    }
}
