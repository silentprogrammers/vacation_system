﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using VacationImplementation.Repository;
using Vacations.API.Models;
using VacationsCore;
using VacationsCore.Repository;
using VacationsCore.Services;

namespace Vacations.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly VacationsCore.ILogger<UsersController> _logger;
        private readonly IMapper _mapper;
        private IVacationsValidationService _validationService;
        private IRepository<User> Repository { get; set; } //= new FakeUserRepository();

        public UsersController(IRepository<User> repository, VacationsCore.ILogger<UsersController> logger, IMapper mapper, IVacationsValidationService validationService)
        {
            _logger = logger;
            _mapper = mapper;
            _validationService = validationService;
            Repository = repository;
        }

        // GET: api/Users
        [HttpGet]
        public IEnumerable<UserModel> Get()
        {
            _logger.Debug("Someone called a IEnumerable<User> Get()");
            var users = Repository.Get();
            IEnumerable<UserModel> userModels = _mapper.Map<IEnumerable<VacationsCore.User>, IEnumerable<UserModel>>(users);
            return userModels;
        }

        // GET: api/Users/5
        //[HttpGet("{id}", Name = "Get")]
        [HttpGet("{id}")]
        public UserModel Get(int id)
        {
            _logger.Debug("Someone called a IEnumerable<User> Get()");
            var user = Repository.GetById(id);
            if (user==null)
                return null;
            var userModel= _mapper.Map<UserModel>(user);
            return userModel;
        }

        // POST: api/Users
        [HttpPost]
        public void Post([FromBody] UserModel userModel)
        {
            _logger.Debug("Someone called a IEnumerable<User> Post()");
            var user = _mapper.Map<User>(userModel);
            if (user != null)
                if (_validationService.ValidateVacations(user.VacationDays))
                    Repository.Insert(user);
        }

        // PUT: api/Users/5
        [HttpPut("{id}")]
        public void Put([FromBody] UserModel userModel)
        {
            _logger.Debug("Someone called a IEnumerable<User> Put()");
            var user = Repository.GetById(userModel.Id);
            if (user == null)
                return; // user doesn't exists
            else
            {
                if (_validationService.ValidateVacations(user.VacationDays))
                {
                    user = _mapper.Map<UserModel, User>(userModel, user);
                    Repository.Update(user);
                }
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _logger.Debug($"Someone called a IEnumerable<User> Delete({id})");
            Repository.Delete(id);
        }
    }
}
