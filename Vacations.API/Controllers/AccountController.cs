﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Vacations.API.ViewModels;
using VacationsCore;
using VacationsCore.Repository;

namespace Vacations.API.Controllers
{
    public class AccountController : Controller
    {
        private readonly VacationsCore.ILogger<UsersController> _logger;
        private readonly IMapper _mapper;
        private IRepository<User> Repository { get; set; }

        public AccountController(IRepository<User> repository, VacationsCore.ILogger<UsersController> logger, IMapper mapper)
        {
            _logger = logger;
            _mapper = mapper;
            Repository = repository;
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginModel model)
        {
            _logger.Debug("Someone called a Login form");
            var user = Repository.Get(u => (u.Email == model.LoginString || u.Logins.Find(x => x.LoginString == model.LoginString) != null) && u.Logins.Find(x => x.PasswordHash == model.Password) != null).FirstOrDefault();
            if (user != null)
            {
                _logger.Debug("Try to Login w/bad keys");
                await Authenticate(model.LoginString);
                return RedirectToAction("Index", "Home");
            }
            ModelState.AddModelError("", "Некорректные логин и(или) пароль");
            return RedirectToAction("Index", "Home");
        }
        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                var user = Repository.Get(u => u.Email == model.Login && u.Logins.Find(x => x.PasswordHash == model.Password) != null).FirstOrDefault();
                if (user == null)
                {
                    //добавляем пользователя в бд
                    List<Login> login = new List<Login>();
                    login.Add(new Login { LoginString = model.Login, PasswordHash = model.Password });
                    Repository.Insert(new User { Email = model.Login, Logins = login, FullName = model.Login });
                    await Authenticate(model.Login); // аутентификация
                    return RedirectToAction("Index", "Home");
                }
                else
                    ModelState.AddModelError("", "Некорректные логин и(или) пароль");
            }
            return View(model);
        }

        private async Task Authenticate(string userName)
        {
            // создаем один claim
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, userName)
            };
            // создаем объект ClaimsIdentity
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            // установка аутентификационных куки
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Login", "Account");
        }
    }
}
