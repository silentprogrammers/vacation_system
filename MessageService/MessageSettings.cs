﻿using System.IO;
using System.Text.Json;
using System.Threading.Tasks;

namespace MessageService
{
    internal class MessageSettings
    {
        public string HostName { get; set; } = "hawk.rmq.cloudamqp.com";
        public string VirtualHost { get; set; } = "hzutqcym";
        public string UserName { get; set; } = "hzutqcym";
        public string Passwords { get; set; } = "l6370CKOHQKUygeKeDb1n628REgg6Z3v";

        public async Task<MessageSettings> GetSettingsAsync()
        {
            string jsonFileName = Directory.GetCurrentDirectory() + "/settings";
            using (FileStream fs = File.OpenRead(jsonFileName))
            {
                return await JsonSerializer.DeserializeAsync<MessageSettings>(fs);
            }
        }

        public async Task SaveSettingsAsync(MessageSettings rqs)
        {
            string jsonFileName = Directory.GetCurrentDirectory() + "/settings";
            using (FileStream fs = File.Create(jsonFileName))
            {
                await JsonSerializer.SerializeAsync(fs, rqs);
            }
        }
    }
}
