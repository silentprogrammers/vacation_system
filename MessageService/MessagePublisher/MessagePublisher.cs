﻿using System.Text;
using Newtonsoft.Json;
using RabbitMQ.Client;
using VacationsCore;
using VacationsCore.MessageQueue;

namespace MessageService.MessagePublisher
{
    public class MessagePublisher : IMessagePublisher
    {
        public void PublishMessage(User user)
        {
            MessageSettings rqs = new MessageSettings();
            var result = rqs.GetSettingsAsync();

            var factory = new ConnectionFactory()
            {
                HostName = rqs.HostName,
                VirtualHost = rqs.VirtualHost,
                UserName = rqs.UserName,
                Password = rqs.Passwords
            };

            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    channel.QueueDeclare(queue: "mainRoute",
                        durable: true,
                        exclusive: false,
                        autoDelete: false,                        
                        arguments: null
                    );                    
                    string serializeMessage = JsonConvert.SerializeObject(user, Formatting.None, new JsonSerializerSettings()
                    {                        
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                    });
                    var body = Encoding.UTF8.GetBytes(serializeMessage);
                    IBasicProperties props = channel.CreateBasicProperties();
                    props.ContentType = "text/plan";
                    props.DeliveryMode = 2;
                    props.Persistent = true;
                    channel.BasicQos(0, 1, false);
                    channel.BasicPublish(exchange: "",
                                        routingKey: "mainRoute",
                                        props ,
                                        body: body);
                }
            }

        }
    }
}
