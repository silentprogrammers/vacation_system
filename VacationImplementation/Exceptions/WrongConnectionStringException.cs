﻿using System;

namespace VacationImplementation.Exceptions
{
    public class WrongConnectionStringException : ApplicationException
    {
        public WrongConnectionStringException(string message)
            : base(message)
        {
            
        }

        public WrongConnectionStringException(string message, Exception inner)
            : base(message, inner)
        {

        }
    }
}
