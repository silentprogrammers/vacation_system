﻿using System;

namespace VacationImplementation.Exceptions
{
    class TooManyDaysVacationsException : ArgumentException
    {
        public int VacationDuration { get; }
        public TooManyDaysVacationsException(string message, int duration)
            : base(message)
        {
            VacationDuration = duration;
        }

        public TooManyDaysVacationsException(string message, int duration, Exception inner)
            : base(message, inner)
        {
            VacationDuration = duration;
        }
    }
}
