﻿using System;

namespace VacationImplementation.Exceptions
{
    public class WrongFilePathException : ApplicationException
    {
        public WrongFilePathException(string message)
            : base(message)
        {
        }

        public WrongFilePathException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
