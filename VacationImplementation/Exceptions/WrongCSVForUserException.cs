﻿using System;

namespace VacationImplementation.Exceptions
{
    public class WrongCSVForUserException : ArgumentException
    {
        public string stringCSV { get; }
        public WrongCSVForUserException(string message, string stringCSV)
            : base(message)
        {
            this.stringCSV = stringCSV;
        }

        public WrongCSVForUserException(string message, string stringCSV, Exception inner)
            : base(message, inner)
        {
            this.stringCSV = stringCSV;
        }
    }
}
