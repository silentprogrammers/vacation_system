﻿using System;

namespace VacationImplementation.Exceptions
{
    class TooFewDaysVacationsException : ArgumentException
    {
        public int VacationDuration { get; }
        public TooFewDaysVacationsException(string message, int duration)
            : base(message)
        {
            VacationDuration = duration;
        }

        public TooFewDaysVacationsException(string message, int duration, Exception inner)
            : base(message, inner)
        {
            VacationDuration = duration;
        }
    }
}
