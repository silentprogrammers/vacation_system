﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using NLog;
using VacationImplementation.Exceptions;
using VacationImplementation.Extensions;
using VacationImplementation.Repository.EFRepository;
using VacationsCore;

namespace VacationImplementation.Serialization
{
    public class CSVSerialization
    {
        private readonly ILogger<CSVSerialization> _logger;

        public CSVSerialization(ILogger<CSVSerialization> logger)
        {
            _logger = logger;
        }

        public string Serialization(List<User> users)
        {
            //Сериализация списка пользователей
            //Сериализация формата csv, параллельная, Task вызванных на ядрах процессора.
            string stringCSV = "";
            int count = users.Count;
            int processorCount = Environment.ProcessorCount;
            int range = count / processorCount;
            List<Task> tasks = new List<Task>();
            for (int p1 = 0; p1 < processorCount; p1++)
            {
                int p = p1;
                int start = p * range;
                int end = (p == processorCount - 1) ? count : start + range;
                Console.WriteLine($"start: {start}, end: {end}");
                Task task = new Task(() =>
                {
                    string stringCSVtask = "";
                    for (int i = start; i < end; i++)
                    {
                        stringCSVtask += users[i].UserToCsv() + Environment.NewLine;
                        Console.WriteLine("Прибавили " + users[i].FullName);
                    }
                    stringCSV += stringCSVtask;
                }
                );
                tasks.Add(task);
                task.Start();
            }
            Task.WaitAll(tasks.ToArray());
            int posNL = stringCSV.LastIndexOf(Environment.NewLine);
            stringCSV = stringCSV.Substring(0, posNL);
            return stringCSV;
        }

        public List<User> Deserialization(string userCSV)
        {
            //Десериализация списка пользователей
            //Десериализация формата csv, параллельная, Task вызванных на ядрах процессора.
            List<User> users = new List<User>();
            string[] usersCSV = userCSV.Split(Environment.NewLine);
            int count = usersCSV.Length;
            int processorCount = Environment.ProcessorCount;
            int range = count / processorCount;
            List<Task> tasks = new List<Task>();

            for (int p1 = 0; p1 < processorCount; p1++)
            {
                int p = p1;
                int start = p * range;
                int end = (p == processorCount - 1) ? count : start + range;
                List<User> usersBlock = new List<User>();
                Task task = new Task(() =>
                {
                    for (int i = start; i < end; i++)
                    {
                        try
                        {
                            User userToAdd = UserFromCsv(usersCSV[i]);
                            User user = UserWithoutId(userToAdd);
                            usersBlock.Add(user);
                        }
                        catch (WrongCSVForUserException exception)
                        {
                            _logger.Error(exception, exception.stringCSV + " .Ошибка! " + exception.Message + ". Запись не будет десериализована!");
                        }
                    }
                    users.AddRange(usersBlock);
                }
                );
                task.Start();
                task.Wait();
            }
            return users;
        }

        public void Deserialization(string connectionString, string pathFileCsv)
        {
            //Десериализация формата csv, параллельная, Task вызванных на ядрах процессора.
            //Десериализуются клиенты репозитория по CSV-файлу. 
            try
            {
                 var userRepository = new UserRepository(connectionString);
                 List<User> users = userRepository.Users;
                 foreach (User user in users)
                 {
                     userRepository.Delete(user);
                 }
                 string[] usersCSV = File.ReadAllLines(pathFileCsv);
                 int count = usersCSV.Length;
                 int processorCount = Environment.ProcessorCount;
                 int range = count / processorCount;
                 List<Task> tasks = new List<Task>();
                
                 for (int p1 = 0; p1 < processorCount; p1++)
                 {
                    int p = p1;
                    int start = p * range;
                    int end = (p == processorCount - 1) ? count : start + range;
                    List<User> usersBlock = new List<User>();
                    Task task = new Task(() =>
                    {
                        for (int i = start; i < end; i++)
                        {
                            try
                            {
                                User userToAdd = UserFromCsv(usersCSV[i]);
                                User user = UserWithoutId(userToAdd);
                                usersBlock.Add(user);
                            }
                            catch (WrongCSVForUserException exception)
                            {
                                _logger.Error(exception, "Запись {0} не будет десериализована. Ошибка {1}",
                                    new { exception.stringCSV, exception.Message });
                            }
                        }
                        userRepository = new UserRepository(connectionString);
                        userRepository.Insert(usersBlock);
                    });
                    task.Start();
                    task.Wait();
                 }
                
            }
            catch (Microsoft.Data.SqlClient.SqlException ex)
            {
                _logger.Error(ex, "Неправильная строка подключения: {0}", connectionString);
                throw new WrongConnectionStringException("Неправильная строка подключения. "+ connectionString, ex);
            }
            catch (ArgumentException ex)
            {
                _logger.Error(ex, "Неправильная строка подключения: {0}", connectionString);
                throw new WrongConnectionStringException("Неправильная строка подключения. " + connectionString, ex);
            }
            catch (DirectoryNotFoundException ex)
            {
                _logger.Error(ex, "Не существует путь, указанный для CSV-файла сериализации: {0}", pathFileCsv);
                throw new WrongFilePathException("Не существует путь, указанный для CSV-файла сериализации. "+ pathFileCsv, ex);
            }
        }

        private User UserFromCsv(string stringCsv)
        {
            //из сериализованного csv фрмата создает объект User
            string[] partsCsv = stringCsv.Split(';');
            string stringExceptionUser = "Строка не соответствует полям User: int Id;string FullName;string Email";
            int userId;
            string userFullName;
            string userEmail;
            int loginId;
            string loginString;
            string loginPasswordHash;
            int vacationDayId;
            string day;
            //Проверка на наличие хотя бы 3 полей User
            if (partsCsv.Length < 3)
            {
                _logger.Error("Строка {0} не соответствует сигнатуре класса {1}.",
                    stringCsv, typeof(User).ToString());
                throw new WrongCSVForUserException(stringExceptionUser, stringCsv);
            }
            //Проверка поле ID целое
            try
            {
                userId = int.Parse(partsCsv[0]);
            }
            catch
            {
                _logger.Error("Строка {0} не соответствует сигнатуре класса {1}. Поле Id не соответствует типу {2}",
                    stringCsv, typeof(User).ToString(), typeof(int).ToString());
                throw new WrongCSVForUserException(stringExceptionUser, stringCsv);
            }
            //Проверка поле FullName не пустое
            if (string.IsNullOrEmpty(partsCsv[1]))
            {
                _logger.Error("Строка {0} не соответствует сигнатуре класса {1}. Поле FullName не может быть пустым",
                    stringCsv, typeof(User).ToString());
                throw new WrongCSVForUserException("Пустое поле FullName", stringCsv);
            }
            else
            {
                userFullName = partsCsv[1].Trim();
            }
            //Проверка поле Email не пустое
            if (string.IsNullOrEmpty(partsCsv[2]))
            {
                _logger.Error("Строка {0} не соответствует сигнатуре класса {1}. Поле Email не может быть пустым",
                    stringCsv, typeof(User).ToString());
                throw new WrongCSVForUserException("Пустое поле Email", stringCsv);
            }
            else
            {
                userEmail = partsCsv[2].Trim();
            }
            User user = new User(userId, userFullName, userEmail);

            int posL = 0;
            for (int i = 0; i < partsCsv.Length; i++)
            {
                if (partsCsv[i] == "L") posL = i;
            }
            if (posL > 0)
            {
                //Есть данные по логинам
                string[] partsL = new string[partsCsv.Length - posL - 1];
                Array.Copy(partsCsv, posL + 1, partsL, 0, partsCsv.Length - posL - 1);

                for (int i = 0; i < partsL.Length; i++)
                {
                    loginId = 0;
                    loginString = "";
                    loginPasswordHash = "";

                    if (i % 3 != 0) continue;
                    if (!int.TryParse(partsL[i], out loginId))
                    {
                        break;
                    }
                    if (!string.IsNullOrEmpty(partsL[i + 1]))
                    {
                        loginString = partsL[i + 1].Trim();
                    }
                    if (!string.IsNullOrEmpty(partsL[i + 2]))
                    {
                        loginPasswordHash = partsL[i + 2].Trim();
                    }
                    if ((loginId > 0) && (loginString != "") && (loginPasswordHash != ""))
                    {
                        Login login = new Login { Id = loginId, LoginString = loginString, PasswordHash = loginPasswordHash, User = user };
                        if (user.Logins == null) 
                            user.Logins = new List<Login>();
                        user.Logins.Add(login);
                    }
                }
            }
            int posV = 0;
            for (int i = 0; i < partsCsv.Length; i++)
            {
                if (partsCsv[i] == "V") posV = i;
            }
            if (posV > 0)
            {
                //Есть данные по отпускным дням
                string[] partsV = new string[partsCsv.Length - posV - 1];
                Array.Copy(partsCsv, posV + 1, partsV, 0, partsCsv.Length - posV - 1);

                for (int i = 0; i < partsV.Length; i++)
                {
                    vacationDayId = 0;
                    day = "";

                    if (i % 2 != 0) continue;
                    if (!int.TryParse(partsV[i], out vacationDayId))
                    {
                        break;
                    }
                    if (!string.IsNullOrEmpty(partsV[i + 1]))
                    {
                        day = partsV[i + 1].Trim();
                    }

                    if ((vacationDayId > 0) && (day != ""))
                    {
                        try
                        {
                            VacationDay vacationDay = new VacationDay
                            {
                                Id = vacationDayId,
                                Day = DateTime.Parse(day),
                                User = user
                            };
                            if (user.VacationDays == null) 
                                user.VacationDays = new List<VacationDay>();
                            user.VacationDays.Add(vacationDay);
                        }
                        catch (FormatException ex)
                        {
                            _logger.Warning("Произошла ошибка парсинга даты {0}. MessageException: {1}. StackTrace: {2}",
                                day, ex.Message, ex.StackTrace);
                        }
                    }

                }
            }
            return user;
        }

        public void Serialization(string connectionString, string pathFileCsv)
        {
            //Сериализуются все клиенты репозитория в файл csv
            //Сериализация формата csv, параллельная, Task вызванных на ядрах процессора.
            try
            {
                var userRepository = new UserRepository(connectionString);
                using (FileStream fstream = new FileStream(pathFileCsv, FileMode.Create))
                {
                    User[] users = userRepository.Users.ToArray();
                    int count = users.Length;
                    int processorCount = Environment.ProcessorCount;
                    int range = count / processorCount;
                    List<Task> tasks = new List<Task>();
                    for (int p1 = 0; p1 < processorCount; p1++)
                    {
                        int p = p1;
                        int start = p * range;
                        int end = (p == processorCount - 1) ? count : start + range;
                        Task task = new Task(() =>
                        {
                            for (int i = start; i < end; i++)
                            {
                                byte[] array = System.Text.Encoding.Default.GetBytes(users[i].UserToCsv() + Environment.NewLine);
                                fstream.WriteAsync(array, 0, array.Length);
                            }
                        }
                        );
                        tasks.Add(task);
                        task.Start();
                    }
                    Task.WaitAll(tasks.ToArray());
                }
            }
            catch (Microsoft.Data.SqlClient.SqlException ex)
            {
                _logger.Error(ex, "Неправильная строка подключения: {0}", connectionString);
                throw new WrongConnectionStringException("Неправильная строка подключения. " + connectionString, ex);
            }
            catch (ArgumentException ex)
            {
                _logger.Error(ex, "Неправильная строка подключения: {0}", connectionString);
                throw new WrongConnectionStringException("Неправильная строка подключения. " + connectionString, ex);
            }
            catch (DirectoryNotFoundException ex)
            {
                _logger.Error(ex, "Не существует путь, указанный для CSV-файла сериализации: {0}", pathFileCsv);
                throw new WrongFilePathException("Не существует путь, указанный для CSV-файла сериализации. " + pathFileCsv);
            }
        }
        
        static User UserWithoutId(User user) 
        {
            //Из экземпляра  юзера убираются Id, так как при insert в таблицу БД присваивает их сама.
            if (user == null) return null;
            User resultUser = new User() { FullName = user.FullName, Email = user.Email };
            if (user.Logins.Count > 0) 
            {
                foreach (Login login in user.Logins)
                {
                    Login loginWithoutId = new Login() { LoginString = login.LoginString, PasswordHash = login.PasswordHash, User = login.User };
                    if (resultUser.Logins == null) 
                        resultUser.Logins = new List<Login>();
                    resultUser.Logins.Add(loginWithoutId);
                }
            }
            if (user.VacationDays.Count > 0)
            {
                foreach (VacationDay vacationDay in user.VacationDays)
                {
                    VacationDay vacationDayWithoutId = new VacationDay() { Day = vacationDay.Day, User = vacationDay.User };
                    if (resultUser.VacationDays == null) 
                        resultUser.VacationDays = new List<VacationDay>();
                    resultUser.VacationDays.Add(vacationDayWithoutId);
                }
            }
            return resultUser;
        }
    }
}
