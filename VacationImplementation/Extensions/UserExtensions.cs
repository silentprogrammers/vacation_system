﻿using System;
using System.Linq;
using VacationsCore;

namespace VacationImplementation.Extensions
{
    public static class UserExtensions
    {
        public static string UserToCsv(this User user)
        {
            string userCsv = "";
            userCsv = $"{user.Id};{user.FullName};{user.Email};";
            if (user.Logins.Any())
            {
                userCsv += "L;"+String.Join(";",user.Logins.Select(t => $"{t.Id};{t.LoginString};{t.PasswordHash}"))+";";
            }
            if (user.VacationDays.Any())
            {
                userCsv += "V;"+String.Join(";",user.VacationDays.Select(t => $"{t.Id};{t.Day}"))+";";
            }
            return userCsv;
        }
    }
}
