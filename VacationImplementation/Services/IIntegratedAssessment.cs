﻿using System.Collections.Generic;
using VacationsCore;

namespace VacationImplementation.Services
{
    public interface IIntegratedAssessment
    {
        int Calc(List<VacationDay> days);
    }
}
