﻿using System;
using System.Collections.Generic;
using System.Linq;
using Logging;
using VacationImplementation.Serialization;
using VacationsCore;
using VacationsCore.Services;

namespace VacationImplementation.Services
{
    public class BasicVacationsValidationService : IVacationsValidationService
    {
        private readonly ILogger<CSVSerialization> _logger = new NLogLogger<CSVSerialization>();
        public static  readonly int MinDaysCount = 28;

        public BasicVacationsValidationService() { }

        public BasicVacationsValidationService(ILogger<CSVSerialization> logger)
        {
            _logger = logger;
        }

        public bool ValidateVacations(List<VacationDay> vacations)
        {
            // TODO: FluentApi
            if (vacations == null) return true; //если у тебя не будет значения - у тебя не возникнет ошибки валидации этого значения
            if (vacations.Count < MinDaysCount) return false;
            if (HasDuplicates(vacations)) return false;

            return true;
        }
        private bool HasDuplicates(List<VacationDay> vacations)
        {
            var allUnique = vacations.GroupBy(x => x.Day).All(g => g.Count() == 1);
            return !allUnique;
        }
    }
}
