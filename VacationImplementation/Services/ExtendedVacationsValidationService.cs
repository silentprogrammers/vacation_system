﻿using System;
using System.Collections.Generic;
using System.Text;
using VacationsCore;
using VacationsCore.Services;

namespace VacationImplementation.Services
{
    public class ExtendedVacationsValidationService : IVacationsValidationService
    {
        private IVacationsValidationService _vacationsValidationServiceImplementation;
        private IIntegratedAssessment _integratedAssessment = new IntegratedAssessment();
        public static readonly int MinCorrectIntegratedAssessment = 30; 

        public ExtendedVacationsValidationService(IVacationsValidationService vacationsValidationServiceImplementation, IIntegratedAssessment integratedAssessment)
        {
            _vacationsValidationServiceImplementation = vacationsValidationServiceImplementation;
            _integratedAssessment = integratedAssessment;
        }
        public ExtendedVacationsValidationService()
        {
            _vacationsValidationServiceImplementation = new BasicVacationsValidationService();
        }

        public bool ValidateVacations(List<VacationDay> vacations)
        {
            var integratedAssessment = _integratedAssessment.Calc(vacations);
            var result = (_vacationsValidationServiceImplementation.ValidateVacations(vacations) && (integratedAssessment >= MinCorrectIntegratedAssessment));
            return result;
        }
    }
}
