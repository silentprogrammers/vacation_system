﻿using System;
using System.Collections.Generic;
using System.Linq;
using EntityFrameworkConnection;
using Microsoft.EntityFrameworkCore;
using VacationsCore;
using VacationsCore.Repository;

namespace VacationImplementation.Repository.EFRepository
{
    public class LoginRepository : IRepository<Login>
    {
        private readonly string _connectionString;
        public LoginRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public List<Login> Logins
        {
            get
            {
                using var context = new Context(_connectionString);
                return context.Logins.Include(l => l.User).ToList();
            }
        }

        public void Delete(Login entityToDelete)
        {
            Delete(entityToDelete.Id);
        }

        public void Delete(int id)
        {
            using var context = new Context(_connectionString);
            var removeLogin = context.Logins.FirstOrDefault(t => t.Id == id);
            if (removeLogin == null)
                throw new ArgumentException("Логин не найден");
            var user = context.Users.Include(u => u.Logins).FirstOrDefault(t => t.Logins.Contains(removeLogin));
            if (user == null)
                throw new ArgumentException("Пользователь для этого логина не найден");
            user.Logins.Remove(removeLogin);
            context.SaveChanges();
        }

        public IEnumerable<Login> Get(Func<Login, bool> predicate = null)
        {
            using var context = new Context(_connectionString);
            return predicate == null
                ? context.Logins.Include(l => l.User).ToList()
                : context.Logins.Include(l => l.User).Where(predicate).ToList();
        }

        public Login GetById(int id)
        {
            using var context = new Context(_connectionString);
            return context.Logins.Include(l => l.User).FirstOrDefault(t => t.Id == id);
        }

        public void Insert(Login entity)
        {
            using var context = new Context(_connectionString);
            var insertLogin = context.Logins.FirstOrDefault(t => t.Id == entity.Id);
            if (insertLogin != null)
                throw new ArgumentException("Такой логин уже есть");
            var user = context.Users.Include(u => u.Logins).FirstOrDefault(t => t.Id == entity.User.Id);
            if (user == null)
                throw new ArgumentException("Пользователь для этого логина не найден");
            user.Logins.Add(entity);
            context.SaveChanges();
        }

        public void Update(Login entityToUpdate)
        {
            using var context = new Context(_connectionString);
            var dbEntity = context.Logins.Single(t => t.Id == entityToUpdate.Id);
            if (dbEntity.User.Equals(entityToUpdate.User))
                throw new ArgumentException("Нельзя переносить логин от пользователя к пользователю");
            dbEntity.LoginString = entityToUpdate.LoginString;
            dbEntity.PasswordHash = entityToUpdate.PasswordHash;
            context.SaveChanges();
        }
    }
}
