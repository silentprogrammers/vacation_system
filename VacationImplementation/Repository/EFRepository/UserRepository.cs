﻿using System;
using System.Collections.Generic;
using System.Linq;
using EntityFrameworkConnection;
using Microsoft.EntityFrameworkCore;
using VacationsCore;
using VacationsCore.Repository;

namespace VacationImplementation.Repository.EFRepository
{
    public class UserRepository : IRepository<User>
    {
        private readonly string _connectionString;

        public List<User> Users
        {
            get
            {
                using var context = new Context(_connectionString);
                return context.Users.
                    Include(t => t.Logins).
                    Include(t => t.VacationDays).
                    ToList();
            }
        }

        public UserRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public void Delete(User entity)
        {
            Delete(entity.Id);
        }

        public void Delete(int id)
        {
            using var context = new Context(_connectionString);
            if (context.Users.FirstOrDefault(t => t.Id == id) == null)
                throw new ArgumentException("Пользователь не найден");
            var removeUser = context.Users.Single(t => t.Id == id);
            context.Users.Remove(removeUser);
            context.SaveChanges();
        }

        public IEnumerable<User> Get(Func<User, bool> predicate = null)
        {
            return predicate == null ? Users : Users.Where(predicate).ToList();
        }

        public User GetById(int id)
        {
            return Get(u => u.Id == id).FirstOrDefault();
        }

        public void Insert(IEnumerable<User> entities)
        {
            using var context = new Context(_connectionString);
            var workEntities = entities.ToList();
            foreach (var entity in workEntities)
            {
                if (context.Users.Contains(entity))
                    throw new ArgumentException($"Пользователь {entity} уже есть в БД");
            }
            context.Users.AddRange(workEntities);
            context.SaveChanges();
        }

        public void Insert(User entity)
        {
            using var context = new Context(_connectionString);
            if (context.Users.Contains(entity))
                throw new ArgumentException("Такой пользователь уже есть");
            Users.Add(entity);
            context.Users.Add(entity);
            context.SaveChanges();
        }

        public void Update(User entityToUpdate)
        {
            using var context = new Context(_connectionString);
            var dbEntityToDelete = context.Users.Single(t => t.Id == entityToUpdate.Id);
            dbEntityToDelete.FullName = entityToUpdate.FullName;
            dbEntityToDelete.Email = entityToUpdate.Email;
            dbEntityToDelete.Logins = new List<Login>(entityToUpdate.Logins);
            dbEntityToDelete.VacationDays = new List<VacationDay>(entityToUpdate.VacationDays);
            context.SaveChanges();
        }
    }
}
