﻿using System;
using System.Collections.Generic;
using System.Linq;
using EntityFrameworkConnection;
using Microsoft.EntityFrameworkCore;
using VacationsCore;
using VacationsCore.Repository;

namespace VacationImplementation.Repository.EFRepository
{
    public class VacationDayRepository : IRepository<VacationDay>
    {
        private readonly string _connectionString;

        public VacationDayRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public List<VacationDay> VacationDays
        {
            get
            {
                using var context = new Context(_connectionString);
                return context.VacationDays.Include(l => l.User).ToList();
            }
        }

        public void Delete(VacationDay entityToDelete)
        {
            Delete(entityToDelete.Id);
        }

        public void Delete(int id)
        {
            using var context = new Context(_connectionString);
            var removeVacationDay = context.VacationDays.FirstOrDefault(t => t.Id == id);
            if (removeVacationDay == null)
                throw new ArgumentException("Отпускной день не найден");
            var user = context.Users.Include(u => u.VacationDays).
                FirstOrDefault(t => t.VacationDays.Contains(removeVacationDay));
            if (user == null)
                throw new ArgumentException("Пользователь для этого отпуска не найден");
            user.VacationDays.Remove(removeVacationDay);
            context.SaveChanges();
        }

        public IEnumerable<VacationDay> Get(Func<VacationDay, bool> predicate = null)
        {
            using var context = new Context(_connectionString);
            return predicate == null
                ? context.VacationDays.Include(vd => vd.User).ToList()
                : context.VacationDays.Include(vd => vd.User).Where(predicate).ToList();
        }

        public VacationDay GetById(int id)
        {
            using var context = new Context(_connectionString);
            return context.VacationDays.Include(vd => vd.User).Single(t => t.Id == id);
        }

        public void Insert(VacationDay entity)
        {
            using var context = new Context(_connectionString);
            var user = context.Users.Include(u => u.VacationDays).FirstOrDefault(t => t.Id == entity.User.Id);
            if (user == null)
                throw new ArgumentException("Пользователь для этого отпуска не найден");
            if (user.VacationDays.Contains(entity))
                throw new ArgumentException("У пользователя в этот день уже назначен отпуск");
            user.VacationDays.Add(entity);
            context.SaveChanges();
        }

        public void Update(VacationDay entityToUpdate)
        {
            using var context = new Context(_connectionString);
            var dbEntity = context.VacationDays.FirstOrDefault(t => t.Id == entityToUpdate.Id);
            if (dbEntity == null)
                throw new ArgumentException("Отпускной день не найден");
            if (!dbEntity.User.Equals(entityToUpdate.User))
                throw new ArgumentException("Нельзя переносить отпуск от пользователя к пользователю");
            dbEntity.Day = entityToUpdate.Day;
            context.SaveChanges();
        }
    }
}
