﻿using System;
using System.Collections.Generic;
using VacationsCore;
using VacationsCore.Repository;

namespace VacationImplementation.Repository
{
    public class FakeUserRepository : IRepository<User>
    {
        private User _fakeUser;

        public FakeUserRepository()
        {
            _fakeUser = new User(666, "John Doe", "JohnDoe@gmail.com");
            _fakeUser.Logins = new List<Login>();
            _fakeUser.Logins.Add(new Login("JohnDoe", "ksjdfhjsugjhlksjdfghlsk", _fakeUser));
            _fakeUser.VacationDays = new List<VacationDay>();
            _fakeUser.VacationDays.Add(new VacationDay(new DateTime(2020, 12, 31), _fakeUser));
        }

        void IRepository<User>.Delete(User entityToDelete)
        {
            //throw new NotImplementedException(); //TODO:
        }

        void IRepository<User>.Delete(int id)
        {
            //throw new NotImplementedException(); //TODO:
        }

        IEnumerable<User> IRepository<User>.Get(Func<User, bool> pred)
        {
            //throw new NotImplementedException(); //TODO:
            return new List<User>() { _fakeUser };
        }

        User IRepository<User>.GetById(int id)
        {
            return _fakeUser;// FakeUser;         
        }
        
        void IRepository<User>.Insert(User entity)
        {
            //throw new NotImplementedException(); //TODO:
        }

        void IRepository<User>.Update(User entityToUpdate)
        {
            //throw new NotImplementedException(); //TODO:
        }
    }
}
