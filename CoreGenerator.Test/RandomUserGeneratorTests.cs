using CoreGenerator;
using NUnit.Framework;

namespace CoreGenerator.Tests
{
    [TestFixture]
    public class RandomUserGeneratorTests
    {
        [Test]
        public void Generate_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            var randomUserGenerator = new RandomUserGenerator();

            // Act
            var result = randomUserGenerator.Generate();

            // Assert
            Assert.Fail();
        }
    }
}
