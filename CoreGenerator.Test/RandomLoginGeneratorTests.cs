using CoreGenerator;
using NUnit.Framework;
using System;

namespace CoreGenerator.Tests
{
    [TestFixture]
    public class RandomLoginGeneratorTests
    {
        [Test]
        public void Generate_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            var randomLoginGenerator = new RandomLoginGenerator();
            User user = null;

            // Act
            var result = randomLoginGenerator.Generate(
                user);

            // Assert
            Assert.Fail();
        }
    }
}
