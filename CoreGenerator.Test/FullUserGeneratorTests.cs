using CoreGenerator;
using NUnit.Framework;
using System;

namespace CoreGenerator.Tests
{
    [TestFixture]
    public class FullUserGeneratorTests
    {
        [Test]
        public void Generate_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            var fullUserGenerator = new FullUserGenerator();
            int dataCount = 0;

            // Act
            var result = fullUserGenerator.Generate(
                dataCount);

            // Assert
            Assert.Fail();
        }

        [Test]
        public void Generate_StateUnderTest_ExpectedBehavior1()
        {
            // Arrange
            var fullUserGenerator = new FullUserGenerator();

            // Act
            var result = fullUserGenerator.Generate();

            // Assert
            Assert.Fail();
        }
    }
}
