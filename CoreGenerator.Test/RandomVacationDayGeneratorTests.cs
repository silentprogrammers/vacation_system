﻿using CoreGenerator;
using NUnit.Framework;
using System;
using VacationsCore;

namespace CoreGenerator.Tests
{
    [TestFixture]
    public class RandomVacationDayGeneratorTests
    {
        [Test]
        public void GenerateFullVacation_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            var randomVacationDayGenerator = new RandomVacationDayGenerator();
            User user = null;

            // Act
            var result = randomVacationDayGenerator.GenerateFullVacation(
                user);

            // Assert
            Assert.Fail();
        }

        [Test]
        public void Generate_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            var randomVacationDayGenerator = new RandomVacationDayGenerator();
            User user = null;
            int dataCount = 0;

            // Act
            var result = randomVacationDayGenerator.Generate(
                user,
                dataCount);

            // Assert
            Assert.Fail();
        }

        [Test]
        public void Generate_StateUnderTest_ExpectedBehavior1()
        {
            // Arrange
            var randomVacationDayGenerator = new RandomVacationDayGenerator();
            User user = null;

            // Act
            var result = randomVacationDayGenerator.Generate(
                user);

            // Assert
            Assert.Fail();
        }
    }
}
