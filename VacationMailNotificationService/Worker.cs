﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MessageClient;
using Microsoft.Extensions.Hosting;
using VacationsCore;
#pragma warning disable 1998

namespace VacationMailNotificationService
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;

        public Worker(ILogger<Worker> logger)
        {
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.Info("Worker running at: {time}", DateTimeOffset.Now);
            MessageReceiver mr = new MessageReceiver();
            try
            {
                mr.GetUsersFromQueue();
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Worker exception at: {time}", DateTimeOffset.Now);
            }
        }
    }
}
