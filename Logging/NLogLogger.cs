﻿using System;
using NLog;
using VacationsCore;

namespace Logging
{
    public class NLogLogger<T> : ILogger<T>
    {
        public Logger Logger { get; set; }

        public NLogLogger()
        {
            Logger = LogManager.GetCurrentClassLogger(typeof(T));
        }

        public void Debug(string message, params object[] formatArgs)
        {
            #if DEBUG
            Logger.Log(LogLevel.Debug, message, formatArgs);
            #endif
        }

        public void Info(string message, params object[] formatArgs)
        {
            Logger.Log(LogLevel.Info, message, formatArgs);
        }

        public void Warning(string message, params object[] formatArgs)
        {
            Logger.Log(LogLevel.Warn, message, formatArgs);
        }

        public void Error(Exception exception, string message, params object[] formatArgs)
        {
            Logger.Log(LogLevel.Error, message, exception, formatArgs);
        }

        public void Error(string message, params object[] formatArgs)
        {
            Logger.Log(LogLevel.Error, message, formatArgs);
        }
    }
}
