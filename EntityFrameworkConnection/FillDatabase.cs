﻿using System.Linq;
using CoreGenerator;

namespace EntityFrameworkConnection
{
    public static class FillDatabase
    {
        public static void FillDatabaseFromNull(string connectionString, int dataCount)
        {
            var context = new Context(connectionString);
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();

            var users = FullUserGenerator.Generate(dataCount);

            context.Users.AddRange(users);
            context.Logins.AddRange(users.SelectMany(t => t.Logins).ToList());
            context.VacationDays.AddRange(users.SelectMany(t => t.VacationDays).ToList());

            context.SaveChanges();
        }
    }
}