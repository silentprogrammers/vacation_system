﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VacationsCore;

namespace EntityFrameworkConnection.Configurations
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("Users").HasKey(p => p.Id);
            builder.Property(p => p.Id).ValueGeneratedOnAdd();
            builder.Property(p => p.FullName).IsRequired();
            builder.Property(p => p.Email).IsRequired();
            builder.HasMany(p => p.Logins).
                WithOne(t => t.User).
                OnDelete(DeleteBehavior.Cascade);
            builder.HasMany(p => p.VacationDays).
                WithOne(t => t.User).
                OnDelete(DeleteBehavior.Cascade);
        }
    }
}