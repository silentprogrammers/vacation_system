﻿using System;
using System.Linq;
using System.Threading;
using CoreGenerator;
using MessageService.MessagePublisher;
using VacationImplementation.Repository.EFRepository;
using VacationsCore;

namespace TestStart
{
    class Program
    {
        /// <summary>
        /// Проект создал для тестирования всего и вся.
        /// Запускаем любой код - смотрим - правим, если надо
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            var connectionString = HardCodeConnectionString.TestConnectionString;

            var userRepository = new UserRepository(connectionString);
            var loginRepository = new LoginRepository(connectionString);
            var vacationDayRepository = new VacationDayRepository(connectionString);
            
            #region User
            //var users = userRepository.Get(t => t.Email.Contains("Igor97"));
            //Console.WriteLine(users.FirstOrDefault());
            //var user = userRepository.GetById(8);
            //Console.WriteLine(user);
            //user = FullUserGenerator.Generate();
            //Console.WriteLine(user);
            //userRepository.Insert(user);
            //user = userRepository.GetById(6);
            //user.Email = "abstract@google.com";
            //user.FullName = "Abstrat User";
            //userRepository.Update(user);
            //userRepository.Delete(userRepository.GetById(12));
            #endregion
            #region Login
            //var logins = loginRepository.Get(t => t.LoginString.Contains("Leonid65"));
            //Console.WriteLine(logins.FirstOrDefault());
            //var login = logins.FirstOrDefault();
            //var user = login.User;
            //var userFromDb = userRepository.GetByID(user.Id);
            //userRepository.Delete(user);
            //var logins = loginRepository.Get(t => t.LoginString.Contains("Alina55"));
            //Console.WriteLine(logins.FirstOrDefault());
            //var login = logins.FirstOrDefault();
            //user = login.User;
            //var newLogin = new Login("Alina65", "akjdshgakjhdglaaskjdhgfasdfg", user);
            //loginRepository.Insert(newLogin);
            #endregion
            #region VacationDay

            #endregion
            #region PublishMessage
            MessagePublisher mp = new MessagePublisher();
            for (int i = 0; i < 50; i++)
            {
                User user1 = FullUserGenerator.Generate();
                Console.WriteLine(user1.ToString());
                mp.PublishMessage(user1);
                Thread.Sleep(1000);
            }
            #endregion


            /*CSVSerialization.Serialization(connectionString, @"D:\C#Education\Files\UserSerialization.csv");
            CSVSerialization.Deserialization(connectionString, @"D:\C#Education\Files\UserSerialization.csv");*/
            Console.ReadKey();
        }
    }
}
