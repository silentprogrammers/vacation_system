﻿namespace TestStart
{
    public static class HardCodeConnectionString
    {
        public static string ConnectionString =>
            string.Join(";",
            "Data Source=79.111.0.195",
            "Initial Catalog=OTUS_Project",
            "User ID=server",
            "Password=E4OyQH6cQEJzanbYb7hi",
            "Application Name=Vacations;");

        public static string TestConnectionString =>
            string.Join(";",
                "Data Source=79.111.0.195",
                "Initial Catalog=OTUS_ProjectTests",
                "User ID=server",
                "Password=E4OyQH6cQEJzanbYb7hi",
                "Application Name=Vacations;");
    }
}
